package org.hsmith.medium.kotlin_build_pipeline
interface Calculator {
    fun sum(number1: Int, number2: Int): Int
}
